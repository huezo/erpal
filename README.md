# erpal

![erpal-web](https://i.imgur.com/x1pLlgE.png)


![erpal1](https://i.imgur.com/WsuRk0t.png)



![erpal-db](https://i.imgur.com/Z3Y6Jph.png)

**MySQL:**


HOST:   ```db```

PORT:   ```3306```




## docker-compose 

```
version: "3"

services:
  erpal:
    image: registry.gitlab.com/huezo/erpal:latest
    ports:
      - "8181:80"
    expose:
      - '8181'
    container_name: erpal
#    depends_on:
#      - mysql
    restart: unless-stopped
    networks:
      - internal
      - external
    volumes:
      - erpal-data:/var/www/html

  db:
    image: mysql:5.5
    command: --max_allowed_packet=32505856      # Set max_allowed_packet to 256M (or any other value)
    container_name: mysql
    restart: always
    environment:
      MYSQL_DATABASE: 'erpal'
      MYSQL_USER: 'erpal'
      MYSQL_PASSWORD: 'erpal'
      MYSQL_ROOT_PASSWORD: 'erpal'
    ports:
      - '3306:3306'
    expose:
      - '3306'
    volumes:
      - mysql-data:/var/lib/mysql'
    networks:
      - internal
      - external


networks:
  external:
    driver: bridge
  internal:
    driver: bridge

volumes:
  erpal-data:
  mysql-data:
```

[docker-compose.yml](docker-compose.yml)

## run

```docker-compose -f docker-compose.yml up -d```


