FROM drupal:7.36

# File Author / Maintainer
MAINTAINER huezo <huezohuezo.1990@gmail.com>

WORKDIR /var/www/html/

RUN apt update 

COPY . /var/www/html/
COPY ./sites/default/default.settings.php   /var/www/html/sites/default/settings.php


RUN chown www-data -R /var/www/html/sites 
